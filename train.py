#!/usr/bin/env python
from xml.etree.ElementTree import ElementTree
from xml.etree.ElementTree import Element
import xml.etree.ElementTree as et
import urllib2
import sc


station = raw_input("Station? ")
print 'Trains from ' + station + '\n'
station = station.lower()
code = sc.stationdict[station]

codeurl = 'http://api.irishrail.ie/realtime/realtime.asmx/getStationDataByCodeXML?StationCode=' + code

tree = et.ElementTree(file=urllib2.urlopen(codeurl))
root = tree.getroot()

for train in root.findall('{http://api.irishrail.ie/realtime/}objStationData'):
	des = train.findtext('{http://api.irishrail.ie/realtime/}Destination')
	arrive = train.findtext('{http://api.irishrail.ie/realtime/}Exparrival')
	print des + ': ' + arrive
